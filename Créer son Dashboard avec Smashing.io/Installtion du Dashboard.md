# Création d'un Dashboard avec [Smashing.io](https://smashing.github.io/)

Créer un Dashboard (tableau de bord en français) avec un Raspberry PI est une bonne manière d'afficher divers informations sur un écran grâce à l'utilisation d'outils nommés Widgets. 

L'avantage de ce Dashboard  est la diversité de Widgets disponibles sur le net. Il est tout à fait possible de réutiliser des Widgets déjà créés par la communauté ou de créer les siens. 

