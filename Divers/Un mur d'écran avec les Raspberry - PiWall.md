
# **PiWall : Présentation et préparation du mur d'écran** 


## 1) Un PiWall, qu'est-ce que c'est ? 

Pas mal de projet ont pour but de créer un mur d'écran, afin de faire une belle présentation ou juste pour recycler du vieux matériel. Pour faire simple, le PiWall est un mur d'écran mis en place grâce à un ou plusieurs Raspberry, et celà ressemble à cette photo : ![exemplepiwall](ressources/piwallexemple.jpg)<br>Voir aussi cette [vidéo de présentation](https://www.youtube.com/watch?v=ym3mnMdEtkg) sur youtube.

 On constate dans cette vidéo qu'il y a deux "types" de Raspberry utilisés : "master" et "slaves" ("maître" et "esclaves" en français). Le Raspberry "master" est celui qui a le contrôle du mur d'écran et qui lance les vidéos. Il y a autant de Raspberry esclaves que d'écrans, c'est ceux-là qui ont la charge de redistribuer l'image du maître sur les écrans. Une petite précision : pour faire un PiWall, il faut utiliser des Raspberry modèle B (le modèle A n'a pas d'interface de réseaux ethernet, nécessaire pour ce projet).

Plusieurs tutos sont disponibles sur le net, mais j'ai travaillé avec ceux-ci : <br>[http://www.piwall.co.uk/information/10-create-your-own-gpl-movie-piwall](http://www.piwall.co.uk/information/10-create-your-own-gpl-movie-piwall) <br>[http://matthewepler.github.io/2016/01/05/piwall.html](http://matthewepler.github.io/2016/01/05/piwall.html)
<br>Certaines images de mes notes ont été prises sur ces sites. 

--- 
## 2) Matériel requis 

- Un ordinateur maître (ici un Raspberry, mais on peut aussi utiliser un ordinateur sous Linux).
- Un Raspberry esclave par écran (fois 3 dans mon cas).
- Un écran compatible pour chaque esclave (les écrans sont branchés avec un câble HDMI).
- Une carte SD par Raspberry pour accueillir un Raspbian, 8Go est largement suffisant (prévoir une carte plus grande pour le maître, selon la taille des fichiers vidéos à diffuser).
- Un switch ethernet avec autant de ports que de Raspberry Pi utilisés.
- Une box 4g 

Ensuite, on est libre d'installer son mur d'écran comme on le souhaite, voici mon installation pour donner un exemple : 

![setup](ressources/setup.jpg)

Pour un mur de 3 écrans, on prévoit donc 4 Raspberry Pi installés sous Raspbian Stretch et un Switch avec au minimum 5 ports, ici on utilise un switch 8 ports PoE de la marque D-Link : 

![switch](ressources/switch.jpg) <br>[Lien amazon](https://www.amazon.fr/D-link-1008P-Commutateur-Ordinateur-bureau/dp/B004FJSST2)

---
## 3) Installations logiciels 

Dans un premier temps, il faut installer Raspbian sur chaque Raspberry (je vous invite à voir sur cette [page](https://gitlab.com/QuentinNardy/framboise/blob/master/documents/Installation%20de%20Rasbian%20Strech.md) avant de commencer le PiWall ! ).

**Un petit point sur les mises à jour sous Linux :** 

Certaines commandes Linux nécessitent d'être exécutées en tant qu'administrateur (root) : on utilise le mot `sudo` avant la commande. Pour rappel, l'invite de commande Linux se nomme "terminal". 

La commande `apt-get` permet de mettre à jour les paquets installés sur la machine. Les paquets sont récupéré sur un serveur miroir et sont souvent mis à jour sur celui-ci. Avant de mettre à jour les paquets de la machine, il faut que la liste des paquets disponibles sur le serveur soit à jour. On éxécute donc les commandes suivantes dans cette ordre : 

Mise à jour de la liste des paquets : 
> sudo apt-get update

Mise à jour des paquets installés sur la machine : 
> sudo apt-get upgrade

Avec `apt-get`, on peut aussi installer de nouveaux paquets : 
> sudo apt-get install nom_du_paquet 

**Installer les paquets nécessaires pour le PiWall :** 

Pour le **master**, il faut installer libav-tools : 
> sudo apt-get install libav-tools 

Les esclaves sont souvent appelés "tuiles" dans les tutoriels ("tile" en anglais). Pour chaque tuile, il faut installer les paquets pwlibs et pwomxplayer. Ils sont disponibles en téléchargement sur le site de Piwall ou en lignes de commandes (avec la commande `wget`). Pour ma part, j'ai téléchargé les paquets directement sur le site. Les 3 actions suivantes sont à répéter sur chaque Raspberry esclaves : 

1. Se rendre le site de PiWall avec chaques tuiles Raspberry : [http://www.piwall.co.uk/](http://www.piwall.co.uk/information/installation)

2. Télécharger les paquets *pwlibs* et *pwomxplayer* dans la section "Installing the software" du site. Ensuite, vérifier le dossier où se trouvent les deux paquets téléchargés (fichier .deb). 

![picture](ressources/lien_paquet.PNG) 

3. Effectuer les commandes suivantes dans le terminal : 

> dpkg -i /home/pi/Downloads/pwlibs1_1.1_armhf.deb 

> dpkg -i /home/pi/Downloads/pwomxplayer_20130815_armhf.deb

---
## 4) Paramétrage des Raspberry en IP fixe : 

A partir d'ici, je nomme les Raspberry esclaves/ tuiles par les noms que je leur ai donné (soit *Rpi1*, *Rpi2* et *Rpi3*). Le Raspberry maitre est nommé *Master*.

On utilisera la commande `nano` pour modifier les fichiers de configuration des Raspberry. `nano` est un éditeur de texte Linux qui fonctionne via le terminal. 

Chaque Raspberry doit avoir une adresse différente (évidemment). J'ai défini les IP de chaque Raspberry en fontion de l'adresse IP du routeur (que l'on retrouve sur l'étiquette). 

![routeur](ressources/etiquette_routeur2.jpg) 

*On remarque que l'ip du routeur est 192.168.1.1* 

Ainsi, les Raspberry auront une adresse IP commençant par `192.168.1.XXX`, voici la liste des adresses que j'ai choisi : 

* Master : 192.168.1.30
* Rpi1 : 192.168.1.31
* Rpi2 : 192.168.1.32
* Rpi3 : 192.168.1.33

![switch](ressources/rpi_ip.jpg)

*Note : il faut penser à bien noter tout ce qui est fait. Comme ça on évite de se perdre !*

Sur chaque Raspberry, on modifie le fichier "/etc/dhcpcd.conf" pour désigner les IP fixes. On éxécute la commande suivante dans le terminal : 

> sudo nano /etc/dhcpcd.conf 

Il faut écrire ces lignes à la fin du fichier "dhcpcd.conf", on modifie l'astérisque (`*`) par le nombre de son choix : 

    # mon adresse ip fixe : 
    interface eth0
    static ip_adresse=192.168.1.*/24
    static routers=192.168.1.1
    static domain_name_servers=192.168.1.1


Après avoir défini les adresses IP fixes, il faut enregistrer les adresses MAC de chaque Raspberry sur le routeur. Cette manipulation peut varier en fonction de votre opérateur ou du modèle du matériel. J'utilise un routeur Huawei, avec l'opérateur Bouygues Télécom. 

On commence par récupérer l'adresse MAC sur chaque Raspberry avec la commande : 

> cat /sys/class/net/eth0/adress 

Sur la page de configuration du routeur que l'on atteint en entrant son adresse ip dans un navigateur, on se connecte en tant qu'admin. Dans l'onglet "Paramètre > DHCP" on entre les adresses MAC des Raspberry avec leur adresses ip fixes choisies plus tôt : 

![interface routeur](ressources/config_dhcp.PNG)

---
## 5) Tester SSH (Secure SHell) 

*Note : Le port SSH par défaut est 22 !* 

On commence par effectuer un test en local. Sur chaque Raspberry, on éxécute la commande : 

> ssh pi@192.168.1.X  

On remplace le "X" par le dernier octet de l'adresse ip fixe du Raspberry utilisé. A l'éxécution de la commande, on tape "yes" puis on entre le mot de passe de l'utilisateur **pi**. S'il n'y a aucun message d'erreur, c'est que la connection SSH est opérationnelle. 

Mais il est posible qu'un message d'erreur apparaisse : `[...] port 22 : connection refused`. C'est le message qui est apparu sur chacun de mes Raspberry. Celà siginfie que le service SSH n'écoute pas sur le port 22. Plusieurs solutions sont possibles et dépendent de la configuration de la machine (paquet manquant, service ssh éteint...). 

Pour ma part, le service SSH était éteint. Je l'ai compris grâce à la commande : 

> service ssh status 

Si il y a "Active : inactive" d'inscrit dans le terminal après l'éxécution de cette commande, celà signifie que le service est bien éteint. Pour l'activer, on effectue les actions suivantes : 

1. Activer SSH dans raspi-config : 
> sudo raspi-config

Puis, aller dans "Interfacing Option" et mettre SSH sur "Enable". 

2. Lancer le service (la commande demande le mot de passe utilisateur) : 
> sudo service ssh start 

3. Refaire le test : 
> ssh pi@192.168.1.X  

*Note : pour tester si l'on a Internet, on ping le DNS de Google qui est toujours activé : `ping 8.8.8.8` (pour stopper la réception des paquets c'est `ctrl + c`).* 

---
## 6) Renommer les Raspberry : 

Avant de commencer la configuration des tuiles, il faut différencier les Raspberry par leur nom d'hôte. Lorsque l'on est dans le terminal, on voit systématiquement `pi@raspberrypi`. Celà signifie que l'utilisateur s'appelle **pi** et la machine se nomme **raspberrypi** (ce sont les noms par défaut). 

Pour l'instant, tous les Raspberry portent le même nom : raspiberrypi. Il faut modifier des fichiers de configurations afin de modifier le nom d'hôte. Pour chaque Raspberry, suivre les étapes : 

1. Modifier les fichiers : 
> sudo nano /etc/hostname 

Il faut effacer le nom "raspberrypi" puis entrer le nouveau nom. 

> sudo nano /etc/hosts 

Il faut remplacer le nom "raspberrypi" par le nom écrit dans le fichier précédent. 

2. Redémarrer la machine : 

Commencer par fermer le terminal, puis ouvrir un autre terminal dans lequel on entre : 
> sudo reboot 

---
## 7)  Configuration des tuiles

Il est temps de sortir papiers, stylos et règles. Dans cette étape, on prend les mesures du mur d'écran pour construire les fichiers de configuration. 

Les fichiers de configuration qui seront créés sont à conserver dans le répertoire personnel de l'utilisateur **pi** (dans : /home/pi).

Toutes les mesures prises sont notées en milimètres. Il faut commencer par faire un schéma de son installation, ressemblant à celui-ci : 

![schema](ressources/schema.PNG)

*Note : Penser à prendre en mesure l'écart entre chaque écran !*

Ensuite, il faut écrire le fichier caché ".piwall". Le point '.' dans le nom du fichier est important, c'est lui qui indique que le fichier est caché. On commence par la commande (avec nano, on crée directement le fichier) : 

> sudo nano /home/pi/.piwall 

Ensuite, il faut entrer les mesures du mur d'écran selon ce format : 

*Précisions : width correspond à l'axe x et height correspond à l'axe y. Les lignes qui commencent par # sont des lignes de commentaires.*

    # définition de la taille totale du mur
    [3bez]
    width=357
    height=62
    x=0
    y=0

    # taille et position de chaque tuile 
    [pi11]
    wall=3bez
    width=112
    height=62
    x=0
    y=0

    [pi12]
    wall=3bez
    width=112
    height=62
    x=124
    y=0

    [pi13]
    wall=3bez
    width=112
    height=62
    x=246
    y=0

Ceci est un exemple du fichier .piwall correspondant au schéma ci-dessus : on y trouve la définition de chaque tuile avec la taille totale du mur. La position de chaque écran est défini par le coin de l'écran qui est en haut à gauche et par la notation x=? et y=? 

Dans cet exemple, le mur d'écran est composé de trois écran posés l'un à côté de l'autre, à l'horizontale. La tuile "pi13" est celle de droite. Sa taille est de 112mm de large et de 62mm de hauteur.  "x=246" : 246 est le total du calcul "112 + 12 + 112 + 12". Il faut prendre en compte l'écart entre chaque écran ! 

## 8) Tester l'installation 



---
##### Commencé par Quentin Nardy le 19/03/2019 
##### Mis à jour le 28/05/2019 