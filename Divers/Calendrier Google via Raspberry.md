---
Réalisation d'un calendrier modifiable à distance. 
Le Raspberry sert uniquement à l'affichage du calendrier, les tâches à afficher sont ajoutées sur le calendrier via le compte Google (sur PC Windows ou Tél Android). 
---

# **Calendrier Google affiché par un Raspberry**

*Le principe est d'afficher Google Agenda au démarrage du Raspberry. Seul un Raspberry PI sous Raspbian est nécessaire pour mettre en place le calendrier.* 

---

### 1) __Installation de Iceweasel__

*Iceweasel* est le même navigateur que *Mozilla Firefox*, il a été renommé par Linux Debian. 

Avant de commencer, penser à mettre à jour le Raspberry (celà permet aussi d'obtenir Iceweasel à la dernière version disponible) : 

    sudo apt-get update
    sudo apt-get upgrade

L'installation se fait en ligne de commande, avec la commande `apt-get` : 

    sudo apt-get install iceweasel

### 2) __Lancement de Google Agenda au démarrage__

Un simple compte Google est nécessaire pour l'utilisation de [Google Agenda](https://www.google.com/intl/fr/calendar/about/ "Aller sur Google Agenda") (Il faut créer une adresse *gmail* si besoin). On se connecte au compte via le Raspberry. 

Tout d'abord, on défini Google Agenda comme étant la page d'accueil par défaut du navigateur. Sur Iceweasel, aller dans les *Préférences* du navigateur, puis dans *Général*. Ensuite, cliquer sur "*Use current page*". 

### 3) __Mettre Iceweasel en plein écran__ 

Le plein écran nécessite un Add-on. Pour l'obtenir, aller dans "*Add-ons* > *Get Add-ons*". Cliquer sur "*Find more Add-ons*", puis chercher "*ForceFull*" dans la barre de recherche. Il suffit d'ajouter cet Add-on et Iceweasel passe en plein écran. 

![ForceFull](ressources/ForceFull.PNG) 

### 4) __Lancer Iceweasel au démarrage du Raspberry__ 

Deux fichiers de configurations sont à modifier pour appliquer le lancement. Pour modifier un fichier en ligne de commande, on utilise la commande `nano`, qui est le nom d'un éditeur de texte en invite de commande. 

Premier fichier : 

    sudo nano /etc/xdg/lxsession/LXDE/autostart 

Il faut ajouter la ligne suivante : 

    @iceweasel -fullscreen

![LXDE autostart](ressources/LXDE_autostart.PNG)

Deuxième fichier : 

    sudo nano /etc/xdg/lxsession/LXDE-pi/autostart 

Il faut ajouter la même ligne que le premier fichier : 

![LXDE autostart](ressources/LXDE-pi_autostart.PNG)

Pour prendre en compte les changements et vérifier que tout fonctionne, il faut redémarrer le Raspberry : `sudo reboot`. Si Iceweasel se lance en plein écran au démarrage, c'est que tout fonctionne ! 

### 5) __Options supplémentaires__ 

#### 5.1) _Empêcher la mise en veille du bureau_

Afin d'éviter que l'écran passe au noir, il faut installer un paquet supplémentaire : 

    sudo apt-get install xscreensaver 

Ce paquet propose un ensemble d'écran de veille (ou économiseurs d'écran) pour tout les systèmes d'exploitation utilisant le serveur graphique `X.org`, dont Raspbian fait parti. 

Ensuite, dans le menu du Raspberry, aller dans *Préférences* > *Economiseur d'écran*. Il faut seulement passer le "Mode" sur "*Désactiver l'économiseur d'écran*". 

![eco ecran](ressources/eco_ecran.PNG)

#### 5.2) _Masquer le curseur de la souris_

Puisque l'on souhaite seulement afficher un écran grâce au Raspberry, on peut souhaiter que le curseur de la souris disparaisse. 

On installe le paquet nécessaire : 

    sudo apt-get install unclutter 

Le paquet a besoin d'être démarré, on lui demande alors de se lancer au démarrage comme pour Iceweasel. On modifie les deux fichiers autosatart : 

    sudo nano /etc/xdg/lxsession/LXDE/autostart
    sudo nano /etc/xdg/lxsession/LXDE-pi/autostart

Et on ajoute la ligne : 

    @unclutter 

Unclutter masque le curseur de la souris au bout de 10 secondes environ (c'est le réglage par défaut). 

###### écrit par Quentin NARDY le 11/07/2019
###### Projet réalisé par Gaëtan ROUX