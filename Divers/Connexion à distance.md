# __La connexion au Raspberry PI à distance__

*Comment se connecter au Raspberry à distance depuis son ordinateur Windows ? Deux méthodes utiles existent, par le bureau à distance de Windows 10 et par le protocole SSH.*

## __Connexion grâce au protocole Secure Shell (SSH)__
___

### 1) *Prérequis*

* Sur l'ordinateur Windows : 

Cet ordinateur est le client dans la relation client/serveur qui sera créé entre les deux machines. On enverra les commandes depuis un client SSH sur Windows et le Raspberry.

L'invite de commande Windows ne permet pas de se connecter via le protocole SSH à un appareil sous Linux. Il faut donc télécharger un outil ou un logiciel le permettant. Il en existe plusieurs, dont Putty qui est beaucoup utilisé sur des tutoriels similaires. 

Ici, on utilise MobaXterm qui propose l'utilisation des commandes Unix/Linux dans un terminal. L'application ne nécessite pas d'installation et peux donc être utilisé depuis une clé USB. 

([Téléchargement1](https://mobaxterm.mobatek.net/download.html)) 
<br>([Téléchargement2](https://www.01net.com/telecharger/windows/Utilitaire/systeme/fiches/114015.html)) 

* Sur le Raspberry : 

Aucune installation n'est nécessaire sur le Raspberry. Il faut seulement veiller à ce que le service SSH soit activé. Pour le vérifier on utilise la commande `service ssh status`. Si le service est activé, la réponse envoyé par le Raspberry est celle-ci : 

![ssh_active](ressources/ssh_active.PNG)

Dans le cas où la réponse affiche *Active: Inactive*, le service est éteint. On peut le lancer avec la commande `service ssh start` ou le relancer avec `service ssh restart` (on vous demande le mot de passe du Raspberry). 

### 2) *Se connecté en ligne de commande via MobaXterm*

Sur le pc Windows, il suffit d'utiliser une simple commande depuis le terminal MobaXterm : 

    ssh <utilisateur>@<adresse_ip_du_raspberry> 

    #Exemple: ssh pi@192.168.1.30


Remarque : il s'agit de la même commande pour qu'un ordinateur sous Linux (qui peut être un autre Raspberry).




## __Utiliser le bureau à distance__
___