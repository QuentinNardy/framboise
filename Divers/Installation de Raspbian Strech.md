

# Raspberry PI : Présentation et installation du système d'exploitation 

Modèle de framboise : *Raspberry PI 3 modèle B+* 

Système d'exploitation : *Raspbian Strecth* version 4.14 

**Liste du matériel nécessaire** : 
- Un Raspberry
- Une carte SD 
- Win32DiskImager 


## 1) Présentation de l'outil : 

Le Raspberry, que l'on traduit littéralement par le mot "framboise" en français, est un nano-ordinateur qui tourne généralement sous le système d'exploitation Linux Raspbian, développé sur la base de Linux Debian. 

*"La dernière version, le Raspberry Pi 3, possède un processeur ARM de 1200 MHz, 4 ports USB, 1 port Ethernet, un module Bluetooth LE, un port HDMI, une prise audio 3.5 mm et un lecteur de carte Micro SD."* [source](https://atomrace.com/presentation-du-raspberry-pi-3/)

On ne s'attarde pas à présenter le Raspberry car celui-ci a déjà été présenté plusieurs fois sur la toile. On passe à l'installation de Raspbian !

## 2) Préparation de la carte SD : 

La taille de la carte SD a peu d'importance, tout de même il est nécessaire d'avoir une taille minimum pour acceuillir Raspbian et les futurs fichiers du Raspberry (Je conseille du 8Go). 

Il est assez facile de préparer sa carte SD pour un Raspberry, il suffit de suivre ces étapes : 

- Connecter la carte SD à un ordinateur
- Télécharger l'image disque du système Raspbian ([download](https://www.raspberrypi.org/downloads/raspbian/)) 
- Télécharger et lancer Win32DiskImager ([download](https://www.01net.com/telecharger/windows/Utilitaire/disque_dur_cdrom_dvd/fiches/136030.html)), qui se présente ainsi : 

![win32](ressources/win32.PNG)

- Cliquer sur le bouton à droite du cadre *"Fichier image"* et sélectionner l'image Raspbian téléchargée plus tôt
- Sélectionner la partition correspondant à la carte SD dans *"Périphérique"* 
- Cliquer sur *"Ecrire"* 

Celà suffit à avoir une carte SD "bootable". 

*(Cette partie n'est peut-être pas nécessaire)*
<br> Il est possible que rien ne se passe lorsque l'on allume le Raspberry après y avoir insérer la carte SD. Pas de panique, il suffit de formater celle-ci. Voici une façon de le faire en ligne de commande Windows avec la méthode Diskpart : 

- Ouvrir l'invite de commande Windows et faire la commande `diskpart` 

Ensuite, suivre la liste des commandes suivantes
    
1) Faire la liste des partitions disponibles : 
    
    > list disk

2) choisir le disque. Sélectionnez celui qui correspond à la carte SD (c'est peut être un autre numéro que 1)

    > select disk 1

3) Vider la partition sélectionnée : 

    > clean

4) Rendre la carte SD à nouveau utilisable : 

    > creat partition primary

    > format fs=ntfs quick

## 3) L'installation de Raspbian : 

Après avoir installer Rasbian sur la carte SD, vous pouvez insérer la carte SD devenue "bootable" dans votre Raspberry. Vous pouvez ensuite allumer celui-ci en branchant le câble d'alimentation, il n'y a pas de bouton on/off sur un Raspberry.

Maintenant, il faut se munir d'un papier et d'un stylo ! Beaucoup d'informations sont à noter pour ne pas les oublier, par example : **les mots de passes** ! 

Dans un premier, vous tombez sur une fenêtre de bienvenue.. seule la suite nous intéresse : 
- **étape 1** : Le choix de la langue. Choisissez le pays, la langue et l'heure. 
- **étape 2** : Le mot de passe utilisateur. Ici, créez votre mot de passe (et pensez à le noter !) 
- **étape 3** : L'accès Wi-Fi. Vous pouvez connecter votre Raspberry PI en Wifi sur votre box. 

C'est terminé pour l'installation du système d'exploitation Raspbian !

<br>Voir aussi : 
<br>- Le site [framboise314](https://www.framboise314.fr/) qui est une source d'information et d'inspiration pour les Raspberry (en français !)

###### Commencé par Quentin Nardy le 18/03/2019
###### Mis à jour le 17/05/2019 